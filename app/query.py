from app import db, models
from sqlalchemy import exc, desc

class query(object):
    def addServer(self, serverName, serverIP, server_os):
        sql = models.servers(name=serverName, ip_address=serverIP, os=server_os, status=0, cpu=True, hdd=True,
                             messages=True, web=True)
        db.session.add(sql)
        try:
            db.session.commit()
        except Exception as error:
            return error
            pass

    def queryServers(self):
        return models.servers.query.all()

    def queryAddToJournal(self):
        return models.journal.query.all()

    def queryNumberOfServers(self):
        return models.servers.query.count()

    def queryNumberOfRows(self):
         return models.cpu_usage.query.count()

    # Запрос таблицы серверов из базы
    def queryServers(self):
        return models.servers.query.all()

    # Запрос строки из таблицы серверов с сортировкой по id
    def querySortById(self, server_id):
        server = models.servers.query.filter_by(id=server_id).first()
        return server

    # Запрос текущей нагрузки на процессор
    def querycpu_usage(self):
        return models.cpu_usage.query.order_by(desc('id')).first()

    # Запрос последних 100 записей нагрузки ЦПУ для построения графиков
    def queryCpuUsage (self, server_id):
        countrows = models.cpu_usage.query.filter_by(server_id=server_id).count()
        if countrows < 1:
            return None
        elif countrows < 100:
            return models.cpu_usage.query.filter_by(server_id=server_id).order_by(desc('id')).limit(20)
        else:
            return models.cpu_usage.query.filter_by(server_id=server_id).order_by(desc('id')).limit(20)

    # Запрос нагрузки на процессор за последние 5 секунд
    def queryCpuUsageLast5sec (self, server_id):
        return models.cpu_usage.query.filter_by(server_id=server_id).order_by(desc('id')).first()
    # Запрос свободного места на HDD
    def queryFreeHddSpace(self, server_id):

        return models.hdd.query.filter_by(server_id=server_id).first()

    # Удаление сервера и всего что с ним связано из базы
    def queryDeleteServer (self, id):
        try:
            models.servers.query.filter_by(id=id).delete()
            models.cpu_usage.query.filter_by(server_id=id).delete()
            models.hdd.query.filter_by(server_id=id).delete()
            models.journal.query.filter_by(server_id=id).delete()
            db.session.commit()
        except Exception as error:
            print (error)
            pass

    # Проверка на существование записи в таблицы с таким айди сервера
    def queryIfExists(self, id):
        exists = models.hdd.query.filter_by(server_id=id).scalar()
        return exists
