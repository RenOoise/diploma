"""Основной исполнительный скрипт приложения
В этом скрипте описываются действия приложения, когда  фиксируется переход пользователя по какой то ссылке. Точнее когда пользователь заходит на какую-то страницу, запускется функция соответствующая этой странице."""

# Снизу идет мопорт библиотек
from app import app, things, query #
#  Из библиотеки app мы импортируем модуль app (само приложение) и модули things и query.
# В модуле query находятся все основные виды запросов к базе, модуль создан для упрощения вызова одинаковых запросов


from hurry.filesize import  size
#импортируем модуль hurry.filesize для расчета размера данных на жестком диске

from flask import request, render_template, flash
#из модуля flask импортируем модули request, render template, flash

from flask_table import Table, Col, LinkCol, ButtonCol
#Из модуля flask_table импортируем Table Col LinkCol и Button Col
#Flask_table - это модуль фласка для создания хтмл таблиц.
# Табле это класс для создания самой таблицы
# Col - это класс для создания простых колонок таблицы
#LinkCol - это класс для создания колонок со ссылками
# и ButtonCol это класс для сощдания колонок с кнопками

from datetime import datetime
#Импортируем модуль для определения времени и ваще работы со временной переменной

import simplejson as json
# Модуль json позволяет передавать json запросы браузеру

from app import db, models
#Тут мы импортируем настройки нашей базы модулем bd и модель нашей базы для работы с ней модулем models

from sqlalchemy import desc
# Здесь импортим из sqlalchemy модуль для сортировки таблицы по убыванию


things = things.things()
# в переменную things заносим класс things() из модуля things

query = query.query()
# в переменную tquery заносим класс query() из модуля query
#@# ЭТО НУЖНО ДЛЯ ПРОСТОГО ВЫЗОВА ФУНКЦИЙ ИЗ КЛАССОВ МОДУЛЕЙ


app.secret_key = 'some_secret'
#это не знаю что, по моему ключ шифрования для хттп обмена


@app.route('/')

@app.route('/index')
#Этими двумя штуками мы начинаем писать функцию для главной страницы. Эти штуки показывают питону, что при переходе пользователя по этим ссылками / и /index надо запускать функцию index() которая ниже
def index():
#Создали функцию индекс
    servers = query.queryServers()
    #Запросили все сервера из базы и запульнули их в переменную
    number_of_servers = query.queryNumberOfServers()
    #Подсчитали сколько серверов в баще
    number_of_rows = query.queryNumberOfRows()
    # Подсчитали сколько строк в таблицах базы


    class ItemTable(Table):
        """Тут в работу включаетсяы модуль flask_table
        Мы создали ттаблицу ItemTable"""
        # Создаем таблицу с информацией о серверах

        # Ниже объявляются свойства таблицы и колонки таблицы, типа id name ip_address os status url
        classes = ['table', 'table-hover']
        id = Col('#')
        name = Col('Имя сервера')
        ip_address = Col("IP-адрес")
        os = Col("Операционная система")
        status = Col('Статус')
        url = LinkCol('Подробно', 'server', url_kwargs=dict(id='id'), attr='name')
    class Item(object):
        #Теперь мы заполняем таблицу переменными из базы
        def __init__(self, id, name, ip_address, status, url, os):
            self.id = id
            self.name = name
            self.ip = ip_address
            self.os = os
            self.status = status
            self.url = url
    # Ниже циклом ФОР начинаем перебор таблицы с серверами построчно и добавляем их в переменную табле
    r=1
    while r < 5:
        for i in servers:
            items = query.queryServers()
            table = ItemTable(items)
            r = r + 1
        server_list =  table.__html__() #Тут помещаем полученный массив в переменную server_list, которую будем передавать уже в html шаблон


    #Журнал ошибок
    errors = models.journal.query.order_by(desc('id')).limit(5)
    number_of_errors = models.journal.query.count()
    #Создаем таблицу с информацией о серверах/  Тут все так же как и в первой таблице, только название другое
    class JournalTable(Table):
        classes = ['table', 'table-hover']
        message = Col('Сообщение')
        error_type = Col("Тип проблемы")
        timestamp = Col("Время")

    class Item(object):
        def __init__(self, id, name, timestamp):
            self.message = message
            self.error_type = error_type
            self.timestamp = timestamp
    r=1
    while r < 5:
        for i in servers:
            items = errors
            table = JournalTable(items)
            r = r + 1
        error_list =  table.__html__()

    #Вот тут самое главное.
    #Запускаем функцию return которая при обращении сервера к функции index() запустит генерацию страницы при помощи функции render_template. Эта функция сгенерирует страницу индекс хтмл и передаст ей переменные которые идут далльше через запятую
    return render_template("index.html", server_list=server_list, number_of_servers=number_of_servers,number_of_rows=number_of_rows,number_of_errors=number_of_errors, error_list=error_list)



# Генерация динамических страниц отдельного сервера.
# Т.е каждый сервер будет иметь собственную страницу создаваемую автоматически по ссылке /variable,
# где variable - урл сервера из базы
#Аналогично функции index() здесь после запроса пользователя страницы /server апустится функция server()
@app.route('/server')
def server():

    # Вот эта штука ниже определяет какой сервер запрошен пользователем, так как ссылка у нас имеет вид server?id=1 , то он определяет чему равен id
    id = request.args.get('id') #если не существует, выдает None
    #Затем к базе полылается запрос с полученным ранее айди и запихививается в переменную server.
    server = query.querySortById(id)

    #Если сервер не существует, то он выдает ошибку 404
    if server == None:
        flash ('Нет такого сервера!')
        return render_template('404.html'), 404
    #Тут мы полученные из базы данных запихиваем в переменные. По названию понятно что и куда запихивается
    server_name = server.name
    server_id = server.id
    server_ip = server.ip_address
    server_status = server.status
    server_os = server.os


    #Запрос места на HDD
    try:
        #Здесь базе посылается запрос о свободном месте на жестком диске
        hdd_free_space = query.queryFreeHddSpace(id)
        if hdd_free_space == None:# если в базе нет такого сервера, или его проверка не ведется то он показывает 0. Ну это такой костыль. Я не успела его решить
            hdd_free_space = 0
        else: #Если же в базе есть такая строка, то свобоное место присваивается в переменную hdd_free_space
            hdd_free_space = hdd_free_space.hdd_usage
            # затем при помощи модуля hurry.filesize, который мы в самом начале импортировали, размер свободного места переводится в гигабайты. ПОтому что в базе он хранится в байтах
            hdd_free_space = size(hdd_free_space)
    except Exception as error:
        print(error)
        pass

    #Строим график
    # Для построения графиков мы создаем 2 списка labels  и values, в которых будут храниться данны о проценте загрузки и о времени соответствующем этому проценту
    legend = 'Загрузка центрального процессора'
    labels = []
    values = []
    try:
        #Посылаем к базе запрос
        get_value = query.queryCpuUsage(id)
        r = 1
        while r < 10:
            #циклом перебираем значения
            for i in get_value:
                cpu_value = i.cpu_value
                time_value = datetime.strftime(i.timestamp, "%H:%M:%S")
                #Добавляем новые значения в наши мыссивы функцией append(значение)
                values.append(cpu_value)
                labels.append(time_value)
                r+=1
    except:
        print("Charts fucked")
        pass


    #Запрос инфы для рассчета загрузки процессора за 5 сек.
    #тут ничего сложного, просто 1 запрос который считает количество строк с соответствующим айди
    # Если строк 0, то он покажет 0 загрузки прцоессора в интерфейса
    #Если строка 1, то он покажет 1 в интерфейсе
    # ЭТО КОСТЫЛЬ!!!!!!!!!!
    # Если строк больше чем одна, возьмет 5 значений и добавит их в массив.
    # Затем посчитает среднее арифметическое и добавит это в переменную cpu
    countrows = models.cpu_usage.query.filter_by(server_id=server_id).count()
    if countrows == 0:
        cpuUsageFor5Sec = 1
    elif countrows == 1:
        cpuUsageFor5Sec = models.cpu_usage.query.filter_by(server_id=server_id).order_by(desc('id')).limit(1)
    else:
        cpuUsageFor5Sec = models.cpu_usage.query.filter_by(server_id=server_id).order_by(desc('id')).limit(countrows)

    if cpuUsageFor5Sec == 0:
        cpu = 0
    else:
        cpuUsageFor5Sec_list = []
        usagecount = 1
        while usagecount < 5:
            for x in cpuUsageFor5Sec:
                cpuUsageFor5Sec = x.cpu_value
                if not cpuUsageFor5Sec:
                    cpuUsageFor5Sec_list.append(0)
                usagecount += 1

        # Вычисляем среднее арифметическое для лоадбара
        cpu = things.arith_mean(cpuUsageFor5Sec_list)
        cpu = round(cpu, 2)

    # тут опять генерируется страница сервер.хтмл, и опять передаются переменные в шаблон
    return render_template("server.html", server_id=server_id, server_name=server_name, server_ip=server_ip, server_status= server_status, server_cpu_usage=cpu, values=values, labels=labels, legend=legend, server_os=server_os, hdd_free_space=hdd_free_space)

@app.route('/servers')
def servers():
    servers = query.queryServers()
    number_of_servers = query.queryNumberOfServers()
    number_of_rows = query.queryNumberOfRows()
    #Создаем таблицу с информацией о серверах
    class ItemTable(Table):
        classes = ['table', 'table-hover']
        id = Col('#')
        name = Col('Имя сервера')
        ip_address = Col("IP-адрес")
        os = Col("Операционная система")
        status = Col('Статус')
        url = LinkCol('Подробно', 'server', url_kwargs=dict(id='id'), attr='name')
    class Item(object):
        def __init__(self, id, name, ip_address, status, url, os):
            self.id = id
            self.name = name
            self.ip = ip_address
            self.os = os
            self.status = status
            self.url = url

    r=1
    while r < 5:
        for i in servers:
            items = query.queryServers()
            table = ItemTable(items)
            r = r + 1
        server_list =  table.__html__()


    class DelTab(Table):
        classes = ['table', 'table-hover']
        name = Col('Имя сервера')
        delbutton = ButtonCol('Удалить', 'deleteServer', button_attrs={'class': 'btn btn-danger'},url_kwargs=dict(id='id'))
    d = 1
    while d < 5:
        for i in servers:
            items = query.queryServers()
            table = DelTab(items)
            d = d + 1
    delbutton =  table.__html__()

    return render_template("servers.html",
        server_list=server_list, number_of_servers=number_of_servers,number_of_rows=number_of_rows, delbutton=delbutton)

#Кнопочка добавления нового сервера в базу
#Тут описывается работа кнопки Добавить сервер. Но страница уже использует http метод POST . Она считывает данные с определенной переменной из шаблона
@app.route('/add_server', methods=["POST"])
def addServer():
    server_name =  request.form['ServerName'];
    server_ip = request.form['ServerIpAddress'];
    server_os = request.form['ServerOS'];
    try:
        #Затем идет добавление сервера в базу
        query.addServer(server_name, server_ip, server_os)
        # И вывод полученных данных в консоль браузера (которая на F12 выывается в хроме)
        return json.dumps({'status': 'OK', 'Servar Name': server_name, 'IP': server_ip });
    except: #Если происходит ошибка, то он в консоль пишет фейл
        pass
        return json.dumps({'status': 'FAIL'});

#Кнопочка удалениея сервера из базы
#Тут все как и выше, тиолько запрос не на добавление, а на удаление
@app.route('/delete_server', methods=["POST"])
def deleteServer():
    server_id = request.form['ServerID'];
# это обработчик ошибки 404 / Если страница не найдена или не существует, то он генерирует при помощи опять же render_template страницу 404.хтмл
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# С логами так же как и с таблицами выше
@app.route('/logs')
def logs():
    #Журнал ошибок
    errors = models.journal.query.order_by(desc('id')).all()
    #Создаем таблицу с информацией о серверах
    class JournalTable(Table):
        classes = ['table', 'table-hover']
        id = Col('ID Ошибки')
        message = Col('Сообщение')
        error_type = Col('Тип ошибки')
        timestamp = Col("Время")
    class Item(object):
        def __init__(self, id, message, error_type, timestamp):
            self.id = id
            self.message = message
            self.error_type = error_type
            self.timestamp = timestamp
    r=1
    while r < 5:
        for i in errors:
            items = errors
            table = JournalTable(items)
            r = r + 1
        error_list =  table.__html__()
    return render_template("logs.html", error_list= error_list)