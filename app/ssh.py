from app import db, models, query, communication
from datetime import datetime
import os
from sqlalchemy import desc

communication = communication.communication()
queryclass = query.query()

query = queryclass.queryServers()


ip_list = []
id_list = []

for i in query:
    ip_list.append(i.ip_address)
    id_list.append(i.id)

def cpu(self, ip_list, id_list):
    ii = 1
    iii = len(id_list)
    while ii < iii:
        ip = ip_list[ii]
        try:
            print(ip)
            server_id = id_list[ii]
            get_value = communication.askServer(ip, 'drop','eb5u8sop', 'python cpu.py')
            time = datetime.utcnow()
            sql = models.cpu_usage(server_id=server_id, cpu_value=get_value, timestamp=time)
            print(server_id)
            print (get_value)
            db.session.add(sql)
            db.session.commit()
        except Exception as e:
            print("Operation error: %s", e)
            pass
        ii+=1
def hdd(self, ip_list, id_list):
    iii = len(id_list)
    ii = 0
    print (iii)
    while ii < iii:
        ip = ip_list[ii]
        print(ip)
        server_id = id_list[ii]
        print(server_id)
        try:
            get_value = communication.askServer(ip, 'drop','eb5u8sop', 'python hddcheck.py')
            time = datetime.utcnow()
            exists = models.hdd.query.filter_by(server_id=server_id).scalar()
            if exists:
                print('Exists')
                sql = models.hdd.query.filter_by(server_id=server_id).first()
                sql.hdd_usage = get_value
                sql.timestamp = time
                db.session.add(sql)
                db.session.commit()

            else:
                print("Not Exists")
                sql = models.hdd(server_id=server_id, hdd_usage=get_value, timestamp=time)
                db.session.add(sql)
                db.session.commit()
        except Exception as e:
            print ("Operation error:", e)
            pass
        ii+=1
def ping (self, ip_list, id_list):
    ping_id = 1
    for value in query:
        ping_host = value.ip_address
        server_id = value.id
        server_name = value.name
        time = datetime.utcnow()
        response = os.system("ping -c 1 " + ping_host)
        if response == 0:
            status = 1
        else:
            status = 0
            #since = datetime.now() - timedelta(hours=24)
            #last_journal = models.journal.query.filter_by(error_description="noconnect").order_by(desc(server_id=server_id).first()
            #if last_journal.error_relevant==True and last_journal.timestamp:

            #query_status = models.journal(server_id=server_id, timestamp=time, error_description = "noconnect", error_relevant = True, message=server_name + " не отвечает!", error_type="critical")
            #db.session.add(query_status)
            #db.session.commit()
        server = models.servers.query.filter_by(id=server_id).first()
        server.status = status
        ping_id = ping_id + 1
        db.session.add(server)
        db.session.commit()

while True:
    hdd(1, ip_list, id_list)
    ping(1, ip_list, id_list)
    cpu(1, ip_list, id_list)
