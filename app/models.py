from app import db

class servers(db.Model):
    id = db.Column(db.Integer, primary_key = True, index = True)
    ip_address = db.Column(db.String(120), index = True, unique = True)
    name = db.Column(db.String(120), index = True, unique = True)
    os = db.Column(db.String(120), index = True)
    status = db.Column(db.Integer, index = True)
    messages = db.Column(db.Boolean, index = True)
    hdd = db.Column(db.Boolean, index = True)
    cpu = db.Column(db.Boolean, index = True)
    web = db.Column(db.Boolean, index = True)

    def __repr__(self):
        return self.id

class cpu_usage(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    server_id = db.Column(db.Integer, db.ForeignKey('servers.id'))
    cpu_value = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime)

    def __repr__(self):
        return self.value

class hdd(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    server_id = db.Column(db.Integer, db.ForeignKey('servers.id'))
    used_space = db.Column(db.Integer)
    hdd_usage = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime)

    def __repr__(self):
        return self.value
        
class services(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    server_id = db.Column(db.Integer, db.ForeignKey('servers.id'))
    port = db.Column(db.Integer)
    status = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime)

class journal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    server_id = db.Column(db.Integer, db.ForeignKey('servers.id'))
    message = db.Column(db.String(560), index = True)
    error_type = db.Column(db.String(120), index = True)
    error_description = db.Column(db.String(120), index = True)
    error_relevant = db.Column(db.Boolean, index = True)
    timestamp = db.Column(db.DateTime)
    def __repr__(self):
        return self.value