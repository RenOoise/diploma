import paramiko

class communication(object):
    """
    Функция опрашивает наждый сервер занесенный в базу по входным данным из таблицы models.servers
    Данные получаем запросом и собираем в массив перебором (циклом for)
    """
    def askServer(self, ip, username, password, command):
        username = username
        password = password
        command = command
        port =  22

        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy)
            client.connect(ip, port=port, username=username, password=password, timeout=1)

            stdin, stdout, stderr = client.exec_command(command)
            get_value = stdout.read()
            get_value = get_value.decode('utf-8')

        finally:
            client.close()
        return get_value